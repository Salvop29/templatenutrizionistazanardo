import {series, watch, parallel} from 'gulp';
import del from 'del';
import buildStyles from './dev/tasks/styles/buildStyle';
import buildStylesProd from './dev/tasks/styles/buildStyleProd';
import buildScripts from './dev/tasks/scripts/buildScripts';
import config from './dev/config';
import buildScriptsProd from "./dev/tasks/scripts/buildScriptsProd";

export const clean = () => del([config.public.public]);

// Watch Task
export const devWatch = () => {
    watch(`${config.src.styles}/**/*.scss`, buildStyles);
    watch(`${config.src.scripts}/**/*.js`, buildScriptsProd);
};

export const dev = series(
    clean,
    parallel(
        buildStyles,
        buildScripts
    ),
    devWatch
);



export const prod = parallel(
    buildStylesProd,
    buildScriptsProd
);

export default series(
    buildStyles,
    buildStylesProd
);