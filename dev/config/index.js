const { resolve } = require('path');
const c_project = 'gabriellazanardo';
const mainPath = resolve(__dirname, '../../');
const themePath = resolve(mainPath, `wp-content/themes`, c_project);

const srcProjectPath = resolve(themePath, 'src');
const publicProjectPath = resolve(themePath, 'public');

module.exports = {
    src: {
        src: publicProjectPath,
        styles: resolve(srcProjectPath, 'styles'),
        scripts: resolve(srcProjectPath, 'scripts'),
        img: resolve(srcProjectPath, 'img')
    },
    public: {
        public: publicProjectPath,
        styles: resolve(publicProjectPath, 'styles'),
        scripts: resolve(publicProjectPath, 'scripts'),
        img: resolve(publicProjectPath, 'img')
    }
};