import {src, dest} from 'gulp';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
import autoprefixer from 'gulp-autoprefixer';
const sass = gulpSass(dartSass);
import config from '../../config';

export default () => src(`${config.src.styles}/**/*.scss`)
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(dest(`${config.public.public}/styles`), { sourcemaps: true });