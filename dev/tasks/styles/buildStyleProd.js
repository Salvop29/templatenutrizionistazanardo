import { src,dest } from 'gulp';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
import gutil from 'gulp-util';
import cleanCss from 'gulp-clean-css';
import gulpRename from 'gulp-rename';
import sourcemaps from 'gulp-sourcemaps';
const sass = gulpSass(dartSass);
import {renameFiles} from '../../utils';
import config from '../../config';

const {
    env: { env }
} = gutil;

export default () => src(`${config.public.styles}/**/*.css`)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(cleanCss())
    .pipe(gulpRename(renameFiles))
    .pipe(dest(config.public.styles));
