import {src, dest} from 'gulp';
import babel from 'gulp-babel';
import config from '../../config';


export default () => src(`${config.src.scripts}/**/*.js`)
    .pipe(babel({
        presets: ['@babel/env']
    }))
    .pipe(dest(config.public.scripts))