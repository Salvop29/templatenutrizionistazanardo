import {src, dest} from 'gulp';
import uglify from "gulp-uglify";
import config from '../../config';
import {renameFiles} from "../../utils";
import gulpRename from "gulp-rename";

export default () => src(`${config.public.scripts}/**/*.js`)
    .pipe(uglify())
    .pipe(gulpRename(renameFiles))
    .pipe(dest(config.public.scripts));