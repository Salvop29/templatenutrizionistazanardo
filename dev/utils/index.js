function renameFiles(path) {
    const {dirname,basename,extname} = path;
    if (basename.indexOf('.min') !== -1) return path;
    return {
        dirname,
        basename: `${basename}.min`,
        extname: extname
    };
}

export {
    renameFiles
}