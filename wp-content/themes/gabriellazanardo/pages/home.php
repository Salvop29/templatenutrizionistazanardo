<?php
/**
 * Template Name: Home
 *
 * @package WordPress
 * @subpackage gabriellazanardo
 * @since gabriellazanardo 1.0
 */
get_header();
?>


    <main class="gz-main">
            <?php
                $fields = get_fields();
                if( $fields ): ?>
                    <?php foreach( $fields as $name => $value ): ?>
                        <?php
                            $section_type = $value['section_type']['label'];
                            get_template_part( 'template-parts/home/content', $section_type, $value );
                         ?>
                    <?php endforeach; ?>
                <?php endif;
            ?>

	</main><!-- #main -->

<?php get_footer();?>
