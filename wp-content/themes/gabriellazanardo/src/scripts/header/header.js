(() => {
    const $menuToggle = jQuery('.gz-header__toggle');
    const $body = jQuery('.gz');
    const $header = jQuery('.gz-header');
    const $href = jQuery('[data-href]');
    $href.on('click', (ev) => {
        const $this = jQuery(ev.target);
        const $data = $this.data('href') || $this.parent().data('href');
        const $target = jQuery(`[data-section="${$data}"]`);
        jQuery([document.documentElement, document.body]).animate({
            scrollTop: $target.offset().top - $header.height()
        });
        if (window.innerWidth < 980 && $body.hasClass('--menu-open')) {
            toggleClasMenuOpen();
        }
    });
    function toggleClasMenuOpen() {
        $body.toggleClass('--menu-open');
    }
    function setPTop() {
        setTimeout(() => {
            $body.css({
                'padding-top': $header.height()
            })
        },100);
    }
    setPTop();
    $menuToggle.on('click', toggleClasMenuOpen);

    jQuery( window ).resize(function() {
        if (window.innerWidth >= 980 && $body.hasClass('--menu-open')) {
            toggleClasMenuOpen();
        }
        setPTop();
    });
})();
