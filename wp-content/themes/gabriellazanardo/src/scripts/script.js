(() => {
    function calcH() {
        // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
        let vh = window.innerHeight * 0.01;
        // Then we set the value in the --vh custom property to the root of the document
        document.documentElement.style.setProperty('--vh', `${vh}px`);
    }

    function calcMInstagram() {
        let elem = jQuery('.m-instagram');
        const ref = jQuery('.gz-instagram__container .gz-text__section-title');
        let rect = ref.offset();
        const { left } = rect;
        elem.css({
            "margin-left": left
        });
    }

    calcMInstagram();
    calcH();


    jQuery( window ).resize(() => {
        calcMInstagram();
        calcH();
    });

    const $accordion = jQuery('.gz-faq__accordion');

    $accordion.on('click', (ev) => {

        ev.preventDefault();
        const { currentTarget } = ev;

        const $currentTarget = jQuery(currentTarget);
        const $body = $currentTarget.find('.gz-faq__accordion-body');
        if ($currentTarget.hasClass('--open')) {
            $body.slideUp();
            $currentTarget.removeClass('--open');
        } else {
            $body.slideDown();
            $currentTarget.addClass('--open');
        }


    });

    new ScrollSpy(function (yPosition, dataSpy) {}, 150);
    jQuery(document).ready(function(){
        jQuery(".gz-sayAboutMe__reviews").slick({
            dots: true,
            arrow: false
        });
    });

})();




