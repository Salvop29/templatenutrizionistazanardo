class ScrollSpy {
    constructor(
        cb = jQuery.noop(),
        trigger = 0
    ) {
        this.cb = cb;
        this.trigger = trigger;
        this.$dataSection = jQuery('[data-section]');
        this.$dataHref = jQuery('[data-href]');
        this.currL = 0;
        this.dataSpy = '';
        this.init();
    }

    init() {
        jQuery(window).scroll((ev) => {
            const $this = window;
            let yPosition = $this.scrollY;
            let cur = this.$dataSection.map((index,el) => {
                if ((jQuery(el).offset().top) < (yPosition + (this.trigger))) {
                    return el;
                }
            });

            if (this.currL != cur.length) {
                this.currL = cur.length;
                cur = cur[cur.length-1];
                this.dataSpy = jQuery(cur).data('section');
                const $filtered = this.$dataHref.filter(`[data-href='${this.dataSpy}']`);
                if ($filtered.length) {
                    this.$dataHref.parent().find('.nav--link.--active').removeClass("--active");
                    if (!$filtered.hasClass('--active')) {
                        $filtered.addClass('--active');
                    }
                }
            }
            return this.cb(yPosition,this.dataSpy,cur);
        });
    }
}
