<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gabriellazanardo
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

     <meta name="description" content="Programmi personali di educazione nutrizionale per ritrovare il peso forma, migliorare la performance sportiva o ottimizzare la composizione corporea.">
      <meta name="keywords" content="nutrizionista, brianza, meda, monza, dimagrire, programmi alimentari, nutrizione, alimentazione, nutrizionista sportivo, nutrizione sport">
      <meta name="author" content="Gabriella Zanardo">
	<?php wp_head(); ?>

	<meta property="og:image" content="https://www.nutrizionistazanardo.it/wp-content/uploads/2022/04/seo-1200x630-1.jpg" />
	<meta property="og:description" content="Programmi personali di educazione nutrizionale per ritrovare il peso forma, migliorare la performance sportiva o ottimizzare la composizione corporea." />
	<meta property="og:type" content="website" />
	<meta property="og:locale" content="it_IT" />
	<meta property="og:site_name" content="Nutrizionista Zanardo" />
	<meta property="og:title" content="Dott.ssa Gabriella Zanardo Biologa Nutrizionista" />
	<meta property="og:url" content="https://nutrizionistazanardo.it/" />

    <meta property="twitter:card" content="summary" />
    <meta property="twitter:title" content="Dott.ssa Gabriella Zanardo Biologa Nutrizionista" />
    <meta property="twitter:description" content="Programmi personali di educazione nutrizionale per ritrovare il peso forma, migliorare la performance sportiva o ottimizzare la composizione corporea." />
    <meta property="twitter:url" content="http://localhost:8000/" />
    <meta itemprop="image" content="https://www.nutrizionistazanardo.it/wp-content/uploads/2022/04/seo-1200x630-1.jpg" />
    <meta itemprop="name" content="Home" />
    <meta itemprop="headline" content="Home" />
    <meta itemprop="description" content="Description" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css" integrity="sha512-yHknP1/AwR+yx26cB1y0cjvQUMvEa2PFzt1c9LlS4pRQ5NOTZFWbhBig+X9G9eYW/8m0/4OXNx8pxJ6z57x0dw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css" integrity="sha512-17EgCFERpgZKcm0j0fEq1YCJuyAWdz9KUtv1EjVuaOz8pDnh/0nZxmU6BBXwaaxqoi9PQXnRWqlcDB027hgv9A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body <?php body_class('gz'); ?>>
<?php wp_body_open(); ?>
<div id="gz-page" class="container-fluid px-0">
	<header id="masthead" class="gz-header position-fixed">
	    <section class="container-fluid px-4 container-lg ">
            <div class="row justify-content-between align-items-center">
                <div class="gz-header__branding col-8 col-lg-3 py-2 py-lg-4">
                    <?php
                        the_custom_logo();
                    ?>
                </div><!-- .site-branding -->
                 <?php if (!is_404()) { ?>
                    <button aria-label="Burger Menu" class="gz-header__toggle col-2 d-flex d-lg-none align-items-center flex-column justify-content-center">
                            <span class=""></span>
                            <span class="my-1"></span>
                            <span class=""></span>
                        </button>
                        <nav class="gz-header__nav col-10 col-lg-7 p-0">
                            <div class="row m-0 justify-content-lg-between align-items-center">
                                <ul class="gz-header__nav-list m-0 pt-6 pt-lg-0 px-0 col-lg-10 col-xl-8 d-lg-flex align-items-lg-center justify-content-lg-around h-100">
                                    <li class="gz-header__nav-image pb-4 pl-6 d-lg-none pt-0">
                                        <img src="<?php echo get_site_icon_url() ?>" alt="WebSite Icon">
                                    </li>
                                    <?php
                                        $menu_items = wp_get_nav_menu_items("Main Menu");
                                        foreach ( $menu_items as $index => $menu_item ):
                                            $id = $menu_item->ID;
                                            $title = $menu_item->title;
                                            $link = $menu_item->url;
                                        ?>
                                        <?php if (str_contains($link, '#')) { ?>
                                            <li class='gz-header__nav-element nav--link py-3 pl-6 p-lg-0' data-href='<?php echo gabriellazanardo_formalize_link($link); ?>'>
                                                <span class='gz-header__nav-link  d-inline-block'  >
                                                    <?php echo $title; ?>
                                                 </span>
                                            </li>
                                         <?php } else { ?>
                                             <li class='gz-header__nav-element py-3 pl-6 p-lg-0'>
                                                 <a class='gz-header__nav-link d-inline-block' href='<?php echo gabriellazanardo_formalize_link($link); ?>' >
                                                    <?php echo $title; ?>
                                                </a>
                                             </li>
                                         <?php } ?>
                                    <?php endforeach; ?>

                                </ul>
                                <ul class="gz-header__social m-0 pt-5 pl-6 pr-0 p-lg-0 col-lg-2 row">
                                    <?php
                                        $menu_social = wp_get_nav_menu_items("Social Menu");
                                        foreach ( $menu_social as $index => $social_item ):
                                            $id = $social_item->ID;
                                            $title = $social_item->title;
                                            $link = $social_item->url;
                                            $col_class = $index % 2 == 0 ? 'col-4 col-sm-2 col-lg-6' : 'col-8 col-sm-2 col-lg-6';
                                            $hide_desk = $index >= 2 ? "d-lg-none" : "";
                                        ?>
                                        <li class='gz-header__social-item px-0 pb-4 pb-lg-0 <?php echo $col_class; ?> <?php echo $hide_desk; ?>'>
                                            <a href='<?php echo $link; ?>' class='--hover-link-image' target='_blank'>
                                                <img width='40' height='40' src='<?php echo gabriellazanardo_social_settings($title, "header"); ?>' alt='Social Icon <?php echo $title; ?>'>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>

                        </nav>
                 <?php } ?>
            </div>
	    </section>



	</header><!-- #masthead -->
