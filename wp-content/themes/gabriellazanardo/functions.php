<?php
/**
 * gabriellazanardo functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package gabriellazanardo
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function gabriellazanardo_setup() {
	/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on gabriellazanardo, use a find and replace
		* to change 'gabriellazanardo' to the name of your theme in all the template files.
		*/
	load_theme_textdomain( 'gabriellazanardo', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	add_theme_support( 'title-tag' );

	/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'menu-1' => esc_html__( 'Primary', 'gabriellazanardo' ),
		)
	);

	/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		)
	);

	// Set up the WordPress core custom background feature.
	add_theme_support(
		'custom-background',
		apply_filters(
			'gabriellazanardo_custom_background_args',
			array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support(
		'custom-logo',
		array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		)
	);
}
add_action( 'after_setup_theme', 'gabriellazanardo_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function gabriellazanardo_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'gabriellazanardo_content_width', 640 );
}
add_action( 'after_setup_theme', 'gabriellazanardo_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function gabriellazanardo_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'gabriellazanardo' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'gabriellazanardo' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'gabriellazanardo_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function gabriellazanardo_scripts() {
    global $wp;
    $srcPath = get_template_directory_uri() . '/public';
    $stylePath = $srcPath . '/styles';
    $scriptsPath = $srcPath . '/scripts';
    $current_url = home_url( $wp->request );

    $list_assets_css = [
        "home" => [
            '/home/hero.css',
            '/home/about.css',
            '/home/sayAboutMe.css',
            '/home/food.css',
            '/home/instagram.css',
            '/home/services.css',
            '/home/faq.css'
        ]
    ];
    $list_assets_css_general = [
        '/style.css',
        '/header/header.css',
        "/footer/footer.css"
    ];

    $list_assets_js_general = [
        '/ScrollSpy/index.js',
        '/script.js',
        '/header/header.js'
    ];

    foreach ($list_assets_css_general as $key=>$value) {
        wp_enqueue_style(
            'gabriellazanardo-style-'.$key,
            $stylePath . $value,
            array(),
            _S_VERSION
        );
    }

    foreach ($list_assets_css as $key=>$value) {
        if(str_contains($current_url, $key) && is_page($key)) {
            foreach($value as $index=>$val) {
                wp_enqueue_style(
                    'gabriellazanardo-style-n-' . $key . '-' . $index,
                    $stylePath . $val,
                    array(),
                    _S_VERSION
                );
            }
        } else if(is_front_page()) {
             foreach($value as $index=>$val) {
                wp_enqueue_style(
                    'gabriellazanardo-style-n-' . $key . '-' . $index,
                    $stylePath . $val,
                    array(),
                    _S_VERSION
                );
            }
        }
    }


	//wp_enqueue_style( 'gabriellazanardo-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'gabriellazanardo-style', 'rtl', 'replace' );

    foreach ($list_assets_js_general as $key=>$value) {
        wp_enqueue_script(
            'gabriellazanardo-script-'.$key,
            $scriptsPath . $value,
            array(),
            _S_VERSION,
            true
        );
    }


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'gabriellazanardo_scripts' );

function gabriellazanardo_social_settings($type, $context = 'header'): string {
    $social_items = [
        "header" => [
            "email"      => "/wp-content/uploads/2022/04/gz-icon-email-1.svg",
            "facebook"   => "/wp-content/uploads/2022/04/gz-icon-fb-1.svg",
            "instagram"  => "/wp-content/uploads/2022/04/gz-icon-ig-1.svg",
            "phone"      => "/wp-content/uploads/2022/04/gz-icon-phone-1.svg",
        ],
        "footer" => [
            "email"      => "/wp-content/uploads/2022/04/gz-icon-email-2.svg",
            "facebook"   => "/wp-content/uploads/2022/04/gz-icon-fb-2.svg",
            "instagram"  => "/wp-content/uploads/2022/04/gz-icon-ig-2.svg",
            "phone"      => "/wp-content/uploads/2022/04/gz-icon-phone-2.svg",
            "maps"       => "/wp-content/uploads/2022/04/gz-icon-pin.svg",
        ]
    ];

    return $social_items[$context][$type];
}

function gabriellazanardo_formalize_link($link): string {
    $specChars = array(
        '_'         => ' ',
        '#'         => '',
        'tel:'      => '',
        'mailto:'   => '',
        ' '         => ' ',
        '%20'       => ' '
    );
    $newString = $link;
    foreach ($specChars as $k => $v) {
        $newString = str_replace($k, $v, $newString);
    }
    return  $newString;
}

//Remove Gutenberg Block Library CSS from loading on the frontend
function smartwp_remove_wp_block_library_css(){
    wp_dequeue_style( 'wp-block-library' );
    wp_dequeue_style( 'wp-block-library-theme' );
    wp_dequeue_style( 'wc-blocks-style' ); // Remove WooCommerce block CSS
}
add_action( 'wp_enqueue_scripts', 'smartwp_remove_wp_block_library_css', 100 );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

