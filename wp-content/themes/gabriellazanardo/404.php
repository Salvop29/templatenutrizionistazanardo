<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package gabriellazanardo
 */
get_header();
?>

	<main id="primary" class="site-main bg-orange-light">
		<section class="gz-404__container not-found container py-6 ">
		    <div class="gz-404__content offset-lg-1 col-lg-11 my-6 row">
		        <h1 class="gz-text__title text-primary">
		            Ooops!
		            <br/>
		            <span class="text-secondary">
		                That page can't be found.
		            </span>
		    </div>

		</section><!-- .error-404 -->

	</main><!-- #main -->

<?php
get_footer();
