"use strict";

(function () {
  var $menuToggle = jQuery('.gz-header__toggle');
  var $body = jQuery('.gz');
  var $header = jQuery('.gz-header');
  var $href = jQuery('[data-href]');
  $href.on('click', function (ev) {
    var $this = jQuery(ev.target);
    var $data = $this.data('href') || $this.parent().data('href');
    var $target = jQuery("[data-section=\"".concat($data, "\"]"));
    jQuery([document.documentElement, document.body]).animate({
      scrollTop: $target.offset().top - $header.height()
    });

    if (window.innerWidth < 980 && $body.hasClass('--menu-open')) {
      toggleClasMenuOpen();
    }
  });

  function toggleClasMenuOpen() {
    $body.toggleClass('--menu-open');
  }

  function setPTop() {
    setTimeout(function () {
      $body.css({
        'padding-top': $header.height()
      });
    }, 100);
  }

  setPTop();
  $menuToggle.on('click', toggleClasMenuOpen);
  jQuery(window).resize(function () {
    if (window.innerWidth >= 980 && $body.hasClass('--menu-open')) {
      toggleClasMenuOpen();
    }

    setPTop();
  });
})();