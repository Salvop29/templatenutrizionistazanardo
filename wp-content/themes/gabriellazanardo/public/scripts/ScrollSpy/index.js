"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

var ScrollSpy = /*#__PURE__*/function () {
  function ScrollSpy() {
    var cb = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : jQuery.noop();
    var trigger = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

    _classCallCheck(this, ScrollSpy);

    this.cb = cb;
    this.trigger = trigger;
    this.$dataSection = jQuery('[data-section]');
    this.$dataHref = jQuery('[data-href]');
    this.currL = 0;
    this.dataSpy = '';
    this.init();
  }

  _createClass(ScrollSpy, [{
    key: "init",
    value: function init() {
      var _this = this;

      jQuery(window).scroll(function (ev) {
        var $this = window;
        var yPosition = $this.scrollY;

        var cur = _this.$dataSection.map(function (index, el) {
          if (jQuery(el).offset().top < yPosition + _this.trigger) {
            return el;
          }
        });

        if (_this.currL != cur.length) {
          _this.currL = cur.length;
          cur = cur[cur.length - 1];
          _this.dataSpy = jQuery(cur).data('section');

          var $filtered = _this.$dataHref.filter("[data-href='".concat(_this.dataSpy, "']"));

          if ($filtered.length) {
            _this.$dataHref.parent().find('.nav--link.--active').removeClass("--active");

            if (!$filtered.hasClass('--active')) {
              $filtered.addClass('--active');
            }
          }
        }

        return _this.cb(yPosition, _this.dataSpy, cur);
      });
    }
  }]);

  return ScrollSpy;
}();