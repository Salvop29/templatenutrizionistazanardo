"use strict";

(function () {
  function calcH() {
    // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
    var vh = window.innerHeight * 0.01; // Then we set the value in the --vh custom property to the root of the document

    document.documentElement.style.setProperty('--vh', "".concat(vh, "px"));
  }

  function calcMInstagram() {
    var elem = jQuery('.m-instagram');
    var ref = jQuery('.gz-instagram__container .gz-text__section-title');
    var rect = ref.offset();
    var left = rect.left;
    elem.css({
      "margin-left": left
    });
  }

  calcMInstagram();
  calcH();
  jQuery(window).resize(function () {
    calcMInstagram();
    calcH();
  });
  var $accordion = jQuery('.gz-faq__accordion');
  $accordion.on('click', function (ev) {
    ev.preventDefault();
    var currentTarget = ev.currentTarget;
    var $currentTarget = jQuery(currentTarget);
    var $body = $currentTarget.find('.gz-faq__accordion-body');

    if ($currentTarget.hasClass('--open')) {
      $body.slideUp();
      $currentTarget.removeClass('--open');
    } else {
      $body.slideDown();
      $currentTarget.addClass('--open');
    }
  });
  new ScrollSpy(function (yPosition, dataSpy) {}, 150);
  jQuery(document).ready(function () {
    jQuery(".gz-sayAboutMe__reviews").slick({
      dots: true,
      arrow: false
    });
  });
})();