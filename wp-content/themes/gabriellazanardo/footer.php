<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gabriellazanardo
 */

?>

	<footer id="colophon" class="gz-footer" data-section="contacts">
        <section class="container-fluid container-lg py-5 py-lg-4 pb-4 px-xl-6">
            <ul class="gz-footer__info row p-0 m-0 justify-content-lg-between py-6 pb-3 position-relative px-xl-6 my-xl-0 pt-0 py-lg-6">
                <?php
                    $menu_social = wp_get_nav_menu_items("Menu Footer");
                    foreach ( $menu_social as $index => $social_item ):
                                $title = $social_item->title;
                                $link = $social_item->url;
                                $col_class = $index % 1 == 0 ? 'px-6' : '';
                                $order = $index != 0 ? $index > 0 && $index == 1 ? 'order-lg-3' : 'order-lg-2' : '';

                                $newLink = gabriellazanardo_formalize_link($link);
                            ?>
                        <li class="gz-footer__info-item px-0 pb-4 pb-lg-0 col-12 text-center col-lg-5 mb-lg-2 <?php echo $col_class ?> <?php echo $order ?>">
                            <a href=<?php echo $link ?> target='_blank' class='gz-footer__info-item--link d-flex flex-column flex-lg-row align-items-center align-items-lg-start text-lg-start'>
                                <img width='20' height='16' class='gz-footer__info-item--img mr-lg-3' src=<?php echo gabriellazanardo_social_settings($title, 'footer'); ?> alt='Footer Info Icon <?php echo $title ?>'>
                                <span class='gz-footer__info-item--label pt-2 pt-lg-0'>
                                    <?php if($title == 'maps') { ?>
                                        Ricevo su appuntamento:<br/>
                                        Corso Italia, 34, 20821 Meda MB
                                    <?php } else { ?>
                                        <?php echo $newLink  ?>
                                    <?php } ?>
                                </span>
                            </a>
                        </li>
                <?php endforeach; ?>
                <li class="gz-footer__separator position-absolute d-none d-lg-block"></li>
            </ul>
            <div class="gz-footer__separator d-lg-none mb-5"></div>
            <div class="gz-footer__follow  text-center">
                <h3 class="gz-footer__follow-title">
                    SEGUIMI SU:
                </h3>
                <ul class="gz-footer__social m-0 pt-1 row px-0 justify-content-center">
                    <?php
                        $menu_social = wp_get_nav_menu_items("Social Menu");
                        foreach ( $menu_social as $index => $social_item ):
                            $title = $social_item->title;
                            $link = $social_item->url;
                            $hide_desk = $index < 2 ? "d-none" : "";
                            $col_class = $index % 2 == 0 ? 'text-end' : 'text-start';
                    ?>
                        <li class='gz-footer__social-item col-1 px-0 mx-1 mx-lg- pb-lg-0 <?php echo $col_class?>  <?php echo $hide_desk?>'>
                            <a href=<?php echo $link?> class='--hover-link-image' target='_blank'>
                                <img width='28' height='28' class='gz-footer__social-item--img' src=<?php echo gabriellazanardo_social_settings($title, 'header');?> alt='Footer Social Icon <?php echo $title?>'>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>



            <div class="row gz-footer__policy text-center justify-content-center align-items-center pt-4">
                <?php
                    $now = new DateTime();
                    $Y = $now->format('Y');
                ?>
                <span class="gz-footer__policy-item p-0">©<?php echo $Y ?> Gabriella Zanardo</span>
                <span class="gz-footer__policy-item sep p-1">|</span>
                <a href="https://www.iubenda.com/privacy-policy/19104428" class=" gz-footer__policy-item p-0 iubenda-nostyle iubenda-white iubenda-noiframe iubenda-embed iubenda-noiframe " title="Privacy Policy ">Policy</a><script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src="https://cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script>

            </div>

        </section>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js" integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</body>
</html>
