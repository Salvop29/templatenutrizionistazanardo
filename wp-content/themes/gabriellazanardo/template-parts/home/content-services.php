
<section class="gz-services __section bg-orange-light text-dark" data-section=<?php echo $args['section_id'];?>>
    <div class="container gz-services__container px-4 py-xl-6">
        <div class="row align-items-center justify-content-between">
            <div class="offset-lg-1 col-lg-5 d-lg-flex d-none">
                <?php get_template_part( 'template-parts/home/services/content-element-services', null, $args['services'] ); ?>
            </div>
            <div class="gz-services__content offset-lg-1 col-lg-5 d-inline-block py-5">
                <div class="gz-text mb-2 mb-lg-4">
                    <span class="gz-text__section-title d-inline-block text-secondary text-uppercase fw-bold mb-4 ">
                        <?php echo $args['section_title'];?>
                    </span>
                    <?php if ($args['title']) { ?>
                        <h2 class="gz-text__subtitle text-primary fw-bold mb-xl-3">
                            <?php echo $args['title'];?>
                        </h2>
                    <?php } ?>
                    <div class="d-lg-none my-4">
                        <?php get_template_part( 'template-parts/home/services/content-element-services', null, $args['services'] ); ?>
                    </div>
                    <p class="gz-text__p pr-xxl-6">
                        <?php echo $args['description'];?>
                    </p>
                </div>
                <a class="--cta -teal -hover-transparent mt-2 mt-xl-4 d-inline-block d-inline-block d-lg-none" target="_blank" href=<?php echo $args['cta']['link_mobile']?>>
                    <?php echo $args['cta']['label']?>
                </a>

                <a class="--cta -teal -hover-transparent mt-2 mt-xl-4 d-none d-lg-inline-block" target="_blank" href=<?php echo $args['cta']['link']?>>
                    <?php echo $args['cta']['label']?>
                </a>
            </div>

        </div>

    </div>
</section>
