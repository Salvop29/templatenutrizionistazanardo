
<ul class="gz-service__element-list p-0 m-0">
    <?php
        $index = 0;
        foreach( $args as $nService => $service ):
            $index++;
            $colReverse = $index % 2 == 0 ? 'flex-row-reverse' : '';
            $p = $index % 2 == 0 ? 'pl-0' : 'pr-0';
        ?>
        <li class="col-12 my-4 my-lg-5">
            <div class="row align-items-center <?php echo $colReverse; ?>">
                <picture class="col-5 <?php echo $p; ?>">
                    <img class="gz-service__element-image" alt="<?php echo $service['label']; ?>" width="150" height="150" src="<?php echo $service['image']; ?>" >
                </picture>
                <span data-index="0<?php echo $index; ?>" class="gz-service__element-label col-6 offset-1 text-uppercase text-primary position-relative">
                    <?php echo $service['label']; ?>
                </span>
            </div>
        </li>
    <?php endforeach; ?>
</ul>
