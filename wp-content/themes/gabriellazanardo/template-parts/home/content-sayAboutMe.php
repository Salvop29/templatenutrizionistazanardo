<?php if ($args['section_title']) { ?>
    <section class="gz-sayAboutMe __section bg-secondary text-white" data-section=<?php echo $args['section_id'];?>>
        <div class="container gz-sayAboutMe__container px-4 py-5 py-lg-6">
            <div class="row justify-content-center">
                <div class="gz-sayAboutMe__content d-inline-block text-center col-md-6 col-xl-4">
                    <div class="gz-text">
                        <span class="gz-text__section-title d-inline-block text-primary text-uppercase fw-bold mb-4">
                            <?php echo $args['section_title'];?>
                        </span>
                        <div class="gz-sayAboutMe__reviews">
                            <?php foreach( $args['reviews'] as $na => $review ): ?>
                                <?php if($review['name']) { ?>
                                    <div class="gz-sayAboutMe__review">
                                       <p class="gz-text__p --sayAboutMe-p text-white">
                                           “<?php echo $review['review'];?>”
                                       </p>
                                       <span class="gz-text__section-title --sayAboutMe-name text-primary text-uppercase fw-bold">
                                           <?php echo $review['name'];?>
                                       </span>
                                    </div>
                                <?php } ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>
<?php } ?>
