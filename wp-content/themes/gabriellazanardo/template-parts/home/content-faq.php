<?php if ($args['section_title']) { ?>
    <section class="gz-faq __section bg-teal-light" data-section=<?php echo $args['section_id'];?>>
        <div class="container gz-faq__container px-4 py-4 py-lg-6">
            <div class="row justify-content-center">
                <div class="gz-faq__content offset-lg-1 col-11 d-inline-block text-left">
                    <div class="gz-text">
                        <span class="gz-text__section-title d-inline-block text-primary text-uppercase fw-bold mb-4">
                            <?php echo $args['section_title'];?>
                        </span>
                        <ul class="gz-faq__element-list p-0 m-0">
                            <?php foreach( $args['faq'] as $nfaq => $faq ): ?>
                                <?php if($faq['question']) { ?>
                                    <li class="gz-faq__accordion col-12 position-relative my-4 text-white pl-4">
                                        <h2 class="gz-faq__accordion-header m-0">
                                            <?php echo $faq['question']; ?>
                                        </h2>
                                        <div class="gz-faq__accordion-body position-relative">
                                            <div class="pl-4 py-2">
                                                 <p class="gz-faq__accordion-p pl-4 m-0" >
                                                    <?php echo $faq['answers']; ?>
                                                </p>
                                            </div>

                                        </div>
                                    </li>
                                <?php } ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>

            </div>

        </div>
    </section>
<?php } ?>
