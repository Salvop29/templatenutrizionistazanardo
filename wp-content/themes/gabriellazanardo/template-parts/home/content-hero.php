<?php if ($args['section_title']) { ?>
    <section class="gz-hero __section bg-lighter text-primary position-relative" data-section=<?php echo $args['section_id'];?>>
        <div class="container gz-hero__container px-lg-4 pb-0">
            <div class="row align-items-center justify-content-center justify-content-lg-between">
                <?php if ($args['image_mob']) { ?>
                    <picture class="gz-hero__image col-lg align-self-end p-0">
                        <?php if ($args['image']) { ?>
                            <source srcset=<?php echo $args['image'];?> media="(min-width: 980px)">
                        <?php } ?>
                        <img width="960" height="980" src=<?php echo $args['image_mob'];?> alt="Hero Main Image">
                    </picture>
                <?php } ?>
                <div class="gz-hero__content col-lg-6 pr-5 d-inline-block py-5 py-lg-6 pl-4 pr-lg-6">
                    <div class="gz-text mb-2 mb-lg-4">
                        <?php if ($args['white_title']) { ?>
                            <h1 class="gz-text__title">
                                <span class="text-primary text-uppercase">
                                    <?php echo $args['white_title'];?>
                                </span>
                                <br>
                                <span class="text-secondary text-uppercase">
                                    <?php echo $args['orange_title'];?>
                                </span>
                            </h1>
                        <?php } ?>
                        <p class="gz-text__p text-primary pr-6 pr-lg-0">
                            <?php echo $args['description'];?>
                        </p>
                    </div>

                    <span class="--cta -hover-transparent -teal mb-6 mt-lg-0 d-inline-block" data-href=<?php echo $args['cta']['link']?>>
                        <?php echo $args['cta']['label']?>
                    </span>
                </div>
            </div>

        </div>
    </section>
<?php } ?>
