<?php if ($args['section_title']) { ?>
    <section class="gz-food __section bg-white text-dark py-2" data-section=<?php echo $args['section_id'];?>>
        <div class="container gz-food__container px-4 py-4 py-lg-6">
            <div class="row">
                <div class="gz-food__container gz-text col-12 col-lg-11 d-lg-flex offset-lg-1">
                    <span class="gz-text__section-title d-inline-block text-primary text-uppercase fw-bold pr-6 mb-4 col-lg">
                        <?php echo $args['section_title'];?>
                    </span>
                    <p class="gz-text__p col-lg-7 offset-lg-1">
                        <?php echo $args['description'];?>
                    </p>
                </div>
                <div class="col-12 col-lg-11 d-lg-flex flex-lg-row-reverse offset-lg-1 align-items-center">
                    <div class="gz-food__container gz-text col-12 my-4 col-lg-7 offset-lg-1">
                        <?php if ($args['title']) { ?>
                            <h2 class="gz-text__subtitle --food-title text-primary fw-normal mb-xl-2">
                                <?php echo $args['title'];?>
                            </h2>
                        <?php } ?>
                        <a class="--cta -teal -hover-white mt-2 mt-xl-4 d-inline-block d-lg-none mb-4" target="_blank" href=<?php echo $args['cta']['link_mobile']?>>
                            <?php echo $args['cta']['label']?>
                        </a>
                        <a class="--cta -teal -hover-white mt-2 d-none d-lg-inline-block mb-4" target="_blank" href=<?php echo $args['cta']['link']?>>
                            <?php echo $args['cta']['label']?>
                        </a>
                    </div>
                    <picture class="gz-food__image col-12 mt-4 col-lg-4 pr-lg-5">
                        <img width="667" height="649"  src=<?php echo $args['image'];?> alt="Food Main Image">
                    </picture>
                </div>

            </div>
        </div>
    </section>
<?php } ?>
