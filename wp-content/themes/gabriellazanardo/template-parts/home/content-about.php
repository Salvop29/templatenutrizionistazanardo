<?php if ($args['section_title']) { ?>
    <section class="gz-about __section bg-white text-dark" data-section=<?php echo $args['section_id'];?>>
        <div class="container gz-about__container px-4 py-xl-6">
            <div class="row align-items-center justify-content-between">
                <div class="offset-lg-1 gz-about__content col-lg-6 d-inline-block py-4">
                    <div class="gz-text mb-2 mb-lg-4">
                        <span class="gz-text__section-title d-inline-block text-secondary text-uppercase fw-bold my-2 mb-4">
                            <?php echo $args['section_title'];?>
                        </span>
                        <?php if ($args['title']) { ?>
                            <h2 class="gz-text__subtitle text-primary fw-bold mb-xl-3">
                                <?php echo $args['title'];?>
                            </h2>
                        <?php } ?>
                        <p class="gz-text__p pr-xxl-6">
                            <?php echo $args['description'];?>
                        </p>
                    </div>

                    <a class="--cta -teal -hover-white mt-2 mt-xl-4 d-inline-block d-lg-none" target="_blank" href=<?php echo $args['cta']['link_mobile']?>>
                        <?php echo $args['cta']['label']?>
                    </a>
                    <a class="--cta -teal -hover-white mt-2 d-none d-lg-inline-block" target="_blank" href=<?php echo $args['cta']['link']?>>
                        <?php echo $args['cta']['label']?>
                    </a>
                </div>
                <picture class="gz-about__image col-lg mt-4 mb-5 d-block">
                    <img width="670" height="670" src=<?php echo $args['image'];?> alt="About Main Image">
                </picture>
            </div>

        </div>
    </section>
<?php } ?>
