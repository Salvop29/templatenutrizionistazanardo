<?php if ($args['section_title']) { ?>
    <section class="gz-instagram __section bg-white position-relative" data-section=<?php echo $args['section_id'];?>>
         <div class="container gz-instagram__container px-4 pt-5 pt-lg-6">
            <div class="row justify-content-center">
                <div class="gz-instagram__content p-0 offset-lg-1 col-11 d-inline-block text-left">
                    <div class="gz-text">
                        <span class="gz-text__section-title d-inline-block text-secondary text-uppercase fw-bold mb-2">
                            <?php echo $args['section_title'];?>
                        </span>

                    </div>
                </div>
            </div>
        </div>
        <div class="overflow-scroll">
            <div class="gz-text__p --instagram-p text-dark">
                <div class="row">
                    <div class="p-0 m-instagram col-11">
                        <?php the_content(); ?>
                    </div>
                </div>

            </div>
        </div>
        <div class="container gz-instagram__container px-4 pb-5 pb-lg-6">
            <div class="row justify-content-center">
                <div class="gz-instagram__content p-0 offset-lg-1 col-11 d-inline-block text-left">
                    <div class="gz-text">
                        <a class="--cta -teal -hover-transparent mt-2 mt-xl-2 d-inline-block" target="_blank" href=<?php echo $args['cta']['link']?>>
                            <?php echo $args['cta']['label']?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
