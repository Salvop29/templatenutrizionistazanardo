const config = require('./dev/config');
const mix = require('laravel-mix');

mix.scripts( `${config.src.scripts}/**/*.js`, `${config.public.scripts}/**/*.js`)
    .sass( `${config.src.styles}/**/*.sass`, config.public.public)
    .copyDirectory(config.src.img, config.public.public)
    .options({
        processCssUrls: false,
    });